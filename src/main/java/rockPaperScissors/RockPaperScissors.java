package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    boolean humanCorrectInput = true;
    String humanInput = "";
    
    public void run() {
    	System.out.println("Let's play round "+ roundCounter);
    	
    	int computerRand = (int)(Math.random()*3)+1;
    	
    	String computerInput = "";
    	if(computerRand == 1) {
    		computerInput = "Rock";
    	}
    	else if(computerRand == 2) {
    		computerInput = "Paper";
    	}
    	else if(computerRand == 3) {
    		computerInput = "Scissors";
    	}
    	
    	while(humanCorrectInput) {
    		humanInput = humanInput();
    		
    		
    		if(!humanInput.equals("")) {
    			humanCorrectInput = false;
    		}
    	}
    	System.out.print("Human chose "+ humanInput + " computer chose "+ computerInput +" ");
    	
    	
    	
    	if((humanInput.equals("Rock")) && (computerInput.equals("Rock"))) {
    		
    		System.out.println("It's a tie!");
    		roundCounter++;
    	}
    	else if((humanInput.equals("Rock")) && (computerInput.equals("Paper"))) {
    		
    		System.out.println("Computer Wins!");
    		computerScore++;
    		roundCounter++;
    	}
    	else if((humanInput.equals("Rock")) && (computerInput.equals("Scissors"))) {
    		
    		System.out.println("Human wins!");
    		humanScore++;
    		roundCounter++;
    	}
    	else if((humanInput.equals("Paper")) && (computerInput.equals("Rock"))) {
    		
    		System.out.println("Human wins!");
    		humanScore++;
    		roundCounter++;
    	}
    	else if((humanInput.equals("Paper")) && (computerInput.equals("Paper"))) {
    		
    		System.out.println("It's a tie!");
    		roundCounter++;
    	}
    	else if((humanInput.equals("Paper")) && (computerInput.equals("Scissors"))) {
    		
    		System.out.println("Computer Wins!");
    		computerScore++;
    		roundCounter++;
    	}
    	else if((humanInput.equals("Scissors")) && (computerInput.equals("Rock"))) {
    		
    		System.out.println("Computer Wins!");
    		computerScore++;
    		roundCounter++;
    	}
    	else if((humanInput.equals("Scissors")) && (computerInput.equals("Paper"))) {
    		
    		System.out.println("Human wins!");
    		humanScore++;
    		roundCounter++;
    	}
    	else if((humanInput.equals("Scissors")) && (computerInput.equals("Scissors"))) {
    		
    		System.out.println("It's a tie!");
    		roundCounter++;
    	}
    	else {
    		System.out.println("I do not understand " + humanInput + ". Could you try again?");
    	}
    	
    	System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    	
    	String finish = readInput("Do you wish to continue playing? (y/n)?");
    	
    	if((finish.equals("y")) || (finish.equals("yes"))) {
    		
    		humanCorrectInput = true;
    		run();
    	}
    	else {
    		System.out.println("Bye bye :)");
    	}
    	
    	
    	
    	
        // TODO: Implement Rock Paper Scissors
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    
    public String humanInput() {
    	String input = readInput("Your choice (Rock/Paper/Scissors)?");
    	String output = "";
    	if((input.equals("Rock")) || (input.equals("rock"))) {
    		output = "Rock";
    	}
    	else if((input.equals("Paper")) || (input.equals("paper"))) {
    		output = "Paper";
    	}
    	else if((input.equals("Scissors")) || (input.equals("scissors"))) {
    		output = "Scissors";
    	}
    	else{
    		System.out.println("I do not understand "+input+". Could you try again?");
    		humanInput();
    		output = "";
    	}
    	return output;
    }

}
